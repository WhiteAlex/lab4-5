﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClassLibrary1.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void test_Teacher_getFullName()
        {
            Teacher teacher = new Teacher("Щедрин", "Родион", "Ильевич");
            Assert.AreEqual("Щедрин Родион Ильевич", teacher.getFullName());
        }

        [TestMethod]
        public void test_Student_getFullName()
        {
            Student student = new Student("Щедрин", "Родион", "Ильевич");
            Assert.AreEqual("Щедрин Родион Ильевич", student.getFullName());
        }
        [TestMethod]
        public void test_Student_journal()
        {
            Student student = new Student("Щедрин", "Родион", "Ильевич");
            Assert.AreEqual("Щедрин Родион Ильевич", student.getFullName());
            student.journal.Add(new Journal(new Lecture("Мат. Анализ"),5));
            Assert.AreEqual("Мат. Анализ", student.journal[0].Lecture.TitleLecture);
            Assert.AreEqual(5, student.journal[0].Mark);

        }

        [TestMethod]
        public void test_DayLessons()
        {
            Lesson lesson = new Lesson(new Lecture("Мат. Анализ"), new Teacher("Щедрин", "Родион", "Ильевич"), new TimeSpan(10,00,00),
                                                                     new TimeSpan(11,00,00), 404);

            Assert.AreEqual("Мат. Анализ", lesson.Lecture.TitleLecture);
            Assert.AreEqual("Щедрин Родион Ильевич",lesson.Teacher.getFullName());
            Assert.AreEqual("10:00:00",lesson.BeginLecture.ToString());
            Assert.AreEqual("11:00:00",lesson.EndLecture.ToString());
            Assert.AreEqual(404, lesson.Room);

        }


    }
}
