﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using ClassLibrary1;

namespace ConsoleApp1
{
    class Program
    {
        
        static void Main(string[] args)
        {
            Logger.InitLogger();
            Student stun = new Student();

            Group group = new Group(123);
            
            stun.FirstName = "Иван";
            stun.MiddleName = "Иванович";
            stun.LastName = "Иванов";
            stun.AddMark(new Journal(new Lecture("Мат. Анализ"),3));
            stun.AddMark(new Journal(new Lecture("Право"), 4));
            stun.AddMark(new Journal(new Lecture("Философия"), 4));
            stun.AddMark(new Journal(new Lecture("Инж.Графика"), 4));
            stun.AddMark(new Journal(new Lecture("История"), 3));
            group.AddStudent(stun);
            stun = new Student();
            stun.FirstName = "Петр";
            stun.MiddleName = "Федорович";
            stun.LastName = "Тфлаф";
            stun.AddMark(new Journal(new Lecture("Мат. Анализ"), 3));
            stun.AddMark(new Journal(new Lecture("Право"), 4));
            stun.AddMark(new Journal(new Lecture("Философия"), 4));
            stun.AddMark(new Journal(new Lecture("Инж.Графика"), 4));
            stun.AddMark(new Journal(new Lecture("История"), 3));
            group.AddStudent(stun);
            stun = new Student();
            stun.FirstName = "Федр";
            stun.MiddleName = "Иванович";
            stun.LastName = "Татыров";
            stun.AddMark(new Journal(new Lecture("Мат. Анализ"), 5));
            stun.AddMark(new Journal(new Lecture("Право"), 5));
            stun.AddMark(new Journal(new Lecture("Философия"), 5));
            stun.AddMark(new Journal(new Lecture("Инж.Графика"), 5));
            stun.AddMark(new Journal(new Lecture("История"), 5));
            group.AddStudent(stun);
            stun = new Student();
            stun.FirstName = "Иван";
            stun.MiddleName = "Иванович";
            stun.LastName = "Москин";
            stun.AddMark(new Journal(new Lecture("Мат. Анализ"), 4));
            stun.AddMark(new Journal(new Lecture("Право")      , 3));
            stun.AddMark(new Journal(new Lecture("Философия")  , 5));
            stun.AddMark(new Journal(new Lecture("Инж.Графика"), 5));
            stun.AddMark(new Journal(new Lecture("История")    , 3));
            group.AddStudent(stun);

            group.ShowStudents();

            foreach (Student s in group.ListStudents)
            {
                s.AllMark();
            }

            Timetable tt = new Timetable();
            DayLesson dayLesson = new DayLesson(new DateTime(2011, 12, 12), 12312);
            dayLesson.AddLesson(new Lesson(new Lecture("Мат. Анализ"), new Teacher("Щедрин", "Родион", "Ильевич"),new TimeSpan(10,00,00), new TimeSpan(11, 00, 00),404));
            dayLesson.AddLesson(new Lesson(new Lecture("Право")      , new Teacher("Пустоходова", "Ирина", "Станиславовна"), new TimeSpan(11, 10, 00), new TimeSpan(12, 20, 00), 404));
            dayLesson.AddLesson(new Lesson(new Lecture("Философия")  , new Teacher("Савватимов", "Вацлав", "Адрианович"), new TimeSpan(12, 20, 00), new TimeSpan(13, 30, 00), 404));
            dayLesson.AddLesson(new Lesson(new Lecture("Инж.Графика"), new Teacher("Печкина", "Регина", "Всеволодовна"), new TimeSpan(13, 30, 00), new TimeSpan(14, 40, 00), 404));
            dayLesson.AddLesson(new Lesson(new Lecture("История"), new Teacher("Кривов", "Кирилл", "Ираклиевич"), new TimeSpan(14, 40, 00), new TimeSpan(15, 50, 00), 404));

            DayLesson dayLesson1 = new DayLesson(new DateTime(2011, 12, 4), 45);
            dayLesson1.AddLesson(new Lesson(new Lecture("Мат. Анализ"), new Teacher("Щедрин", "Родион", "Ильевич"), new TimeSpan(10, 00, 00), new TimeSpan(11, 00, 00), 404));
            dayLesson1.AddLesson(new Lesson(new Lecture("Право"), new Teacher("Пустоходова", "Ирина", "Станиславовна"), new TimeSpan(11, 10, 00), new TimeSpan(12, 20, 00), 404));
            dayLesson1.AddLesson(new Lesson(new Lecture("Философия"), new Teacher("Савватимов", "Вацлав", "Адрианович"), new TimeSpan(12, 20, 00), new TimeSpan(13, 30, 00), 404));
            dayLesson1.AddLesson(new Lesson(new Lecture("Инж.Графика"), new Teacher("Печкина", "Регина", "Всеволодовна"), new TimeSpan(13, 30, 00), new TimeSpan(14, 40, 00), 404));
            dayLesson1.AddLesson(new Lesson(new Lecture("История"), new Teacher("Кривов", "Кирилл", "Ираклиевич"), new TimeSpan(14, 40, 00), new TimeSpan(15, 50, 00), 404));


            tt.AddDay(dayLesson);
            tt.AddDay(dayLesson1);
            foreach (DayLesson d in tt.DayLessons)
            {
                Console.WriteLine(d.Date.Date);
                d.ShowLessons();
            }

            Console.ReadLine();
        }
    }
}
