﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
   public class DayLesson
   {
       public DateTime Date { get; set; }
       public List<Lesson> lessons { get; set; }
       public int NumGroup { get; set; }


       public DayLesson() { this.lessons = new List<Lesson>(); }
       public DayLesson(DateTime date, int group)
       {
           this.lessons = new List<Lesson>();
           this.Date = date;
           this.NumGroup = group;
       }

       public void AddLesson(Lesson lesson)
       {
           Logger.Log.Info("Добавлена запись: " +
               string.Format("{0} {1}:{2}",lesson.Lecture.TitleLecture,
                                            lesson.BeginLecture.Hours, lesson.EndLecture.Minutes));
            lessons.Add(lesson);
       }

        public void ShowLessons()
        {
            foreach(Lesson l in lessons)
            {
                Console.WriteLine("{0} - {1} {2} {3} {4}", l.BeginLecture.ToString(),
                                                       l.EndLecture.ToString(),
                                                       l.Lecture.TitleLecture,
                                                       l.Room,
                                                       l.Teacher.getFullName());
            }
        }
   }
}
