﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Journal
    {
        public Lecture Lecture { get; set; }
        public int Mark { set; get; }

        public Journal() { }
        public Journal(Lecture lec, int mark)
        {
            this.Lecture = lec;
            this.Mark = mark;
            Logger.Log.Info("\\Создан экземпляр//");
        }
    }
}
