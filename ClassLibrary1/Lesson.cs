﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Lesson
    {
       
        public Lecture  Lecture { get; set; }
        public TimeSpan BeginLecture { get; set; }
        public TimeSpan EndLecture { get; set; }
        public Teacher  Teacher { get; set; }
        public int Room { get; set; }
        

        public Lesson() { }
        public Lesson(Lecture lecture, Teacher teacher, TimeSpan beginTime, TimeSpan endTime,int room)
        {
            this.Lecture = lecture;
            this.Teacher = teacher;
            this.BeginLecture = beginTime;
            this.EndLecture = endTime;
            this.Room = room;
        }

    }
}
