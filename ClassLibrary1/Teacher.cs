﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Teacher
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">ID</param>
        /// <param name="fn">FirstName</param>
        /// <param name="mn">MiddleName</param>
        /// <param name="ln">LastName</param>
        public Teacher() { }
        public Teacher(int id, string ln, string fn, string mn)
        {
            this.ID = id;
            this.FirstName = fn;
            this.MiddleName = mn;
            this.LastName = ln;
            Logger.Log.Info("\\Создан экземпляр//");
        }
        public Teacher(string ln, string fn, string mn)
        {
            
            this.FirstName = fn;
            this.MiddleName = mn;
            this.LastName = ln;
            Logger.Log.Info("\\Создан экземпляр//");
        }

        public string getFullName()
        {
            return string.Format("{0} {1} {2}", LastName, FirstName, MiddleName);
        }

    }
}
