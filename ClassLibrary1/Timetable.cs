﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;

namespace ClassLibrary1
{
    public class Timetable
    {
        public List<DayLesson> DayLessons { get; set; }
        public Timetable() { DayLessons = new List<DayLesson>(); }
        
        public void AddDay(DayLesson day)
        {
            DayLessons.Add(day);
            Logger.Log.Info("Добавлено: "+ day.NumGroup.ToString() +
                day.Date.ToString());
        }


    }
}
