﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Student
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public List<Journal> journal { get; set; }

        public Student() { journal = new List<Journal>(); }
        public Student(string ln, string fn, string mn)
        {
            this.FirstName = fn;
            this.MiddleName = mn;
            this.LastName = ln;
            journal = new List<Journal>();
            Logger.Log.Info("\\Создан экземпляр//");
        }
        public Student(int id, string ln, string fn, string mn)
        {
            this.ID = id;
            this.FirstName = fn;
            this.MiddleName = mn;
            this.LastName = ln;
            journal = new List<Journal>();
            Logger.Log.Info("\\Создан экземпляр//");
        }

        public void AllMark()
        {
            Logger.Log.Info("Студент показывает свои оценки");
            Console.WriteLine(getFullName());
            foreach (Journal j in journal)
            {
                Console.WriteLine("{0} {1}",j.Lecture.TitleLecture, j.Mark);
            }
        }

        public void AddMark(Journal j)
        {
            journal.Add(j);

            Logger.Log.Info("Добавление оценки" + string.Format("{0} {1}", j.Lecture.TitleLecture, j.Mark) );
        }

        public string getFullName()
        {
            return string.Format("{0} {1} {2}", LastName, FirstName, MiddleName);
        }
    }
}
