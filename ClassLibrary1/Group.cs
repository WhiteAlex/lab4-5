﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Group
    {
        public int NumberGroup { get; set; }
        public List<Student> ListStudents { get; set; }

        public Group(){ ListStudents = new List<Student>(); }
        public Group(int num)
        {
            this.NumberGroup = num;
            ListStudents = new List<Student>();
        }

        public void AddStudent(Student student)
        {
            ListStudents.Add(student);
            Logger.Log.Info("Добавлена запись: " +
                string.Format("{0} {1} {2}", student.LastName,
                                             student.MiddleName,
                                             student.FirstName));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// if id not found then
        /// <returns>null</returns>
        public Student FindStudent(int id)
        {
            foreach (Student s in ListStudents)
            {
                if (s.ID == id)
                {
                    Logger.Log.Info("Добавлена запись: " +
                string.Format("{0} {1} {2}", s.LastName,
                                             s.MiddleName,
                                             s.FirstName));
                    return s;
                }
            }
            return null;
        }

        public void ShowStudents()
        {
            Logger.Log.Info("");
            foreach (Student s in ListStudents)
            {
                Console.WriteLine();
            }
        }
    }
}
