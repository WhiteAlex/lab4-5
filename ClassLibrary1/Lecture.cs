﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;

namespace ClassLibrary1
{
    public class Lecture
    {
        public string TitleLecture { get; set; }
        public Lecture() { this.TitleLecture = ""; }
        public Lecture(string title) { this.TitleLecture = title; }

    }
}
